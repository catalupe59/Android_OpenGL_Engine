package com.brawlers.cata.brawlers2.Component.LowLevel.Structs;

/**
 * Created by Cata on 9/20/2017.
 */

public class Vector2f {
    public float x;
    public float y;

    public Vector2f(){
        x = 0;
        y = 0;
    }
    public Vector2f(float x){
        this.x = x;
        this.y = 0;
    }
    public Vector2f(float x, float y){
        this.x = x;
        this.y = y;
    }
}
