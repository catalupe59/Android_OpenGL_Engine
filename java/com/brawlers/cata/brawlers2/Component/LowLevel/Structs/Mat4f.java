package com.brawlers.cata.brawlers2.Component.LowLevel.Structs;

import android.util.Log;

/**
 * Created by Cata on 9/8/2017.
 */

public class Mat4f {
    private float [] data;

    public Mat4f(float [] data){
        this.data = data;
    }

    public static Mat4f identity(){
        Mat4f mat = new Mat4f(new float[]{  1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1});
        return mat;
    }
    public void Product(float scalar){
        for (int i =0 ; i < data.length; i++){
            data[i] *= scalar;
        }
    }
    public static Mat4f Product(Mat4f mat1, Mat4f mat2){
        Mat4f result = new Mat4f(new float [] {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0});
        for (int i =0; i < 4; i++){
            for (int j = 0; j <4 ;j++){
                for (int k = 0; k < 4; k++){
                    result.data[i*4 + j] += mat1.data[i*4+k] * mat2.data[k*4 + j];
                }
            }
        }
        return result;
    }
    public void Identity(){
        data = new float[]{  1,0,0,0,
                0,1,0,0,
                0,0,1,0,
                0,0,0,1};
    }
    public void Scale(Vector3f vec){
        Identity();
        data[0] = vec.x;
        data[5] = vec.y;
        data[10] = vec.z;
    }
    public void Translate(Vector3f vec){
        Identity();
        data[3] = vec.x;
        data[7] = vec.y;
        data[11] = vec.z;

    }
    public static Mat4f ScaleSt(Vector3f vec){
        Mat4f mat = Mat4f.identity();
        mat.data[0] = vec.x;
        mat.data[5] = vec.y;
        mat.data[10] = vec.z;
        return mat;
    }
    public static Mat4f TranslateSt(Vector3f vec){
        Mat4f mat = identity();

        mat.data[3] = vec.x;
        mat.data[7] = vec.y;
        mat.data[11] = vec.z;
        return mat;
    }

    public static Mat4f RotateSt(float angle){
        Mat4f mat = identity();
        mat.data[0] = (float)Math.cos(angle);
        mat.data[1] = (float)-Math.sin(angle);
        mat.data[4] = (float)Math.sin(angle);
        mat.data[5] = (float)Math.cos(angle);
        return mat;
    }

    public float[] getData(){
        return data;
    }

    public Mat4f Transpose(){
        float data[] = {
                this.data[0], this.data[4], this.data[8], this.data[12],
                this.data[1], this.data[5], this.data[9], this.data[13],
                this.data[2], this.data[6], this.data[10], this.data[14],
                this.data[3], this.data[7], this.data[11], this.data[15]
        };
        Mat4f mat = new Mat4f(data);
        return mat;
    }

    @Override
    public String toString() {
        String rezult = "";
        for (int i = 0 ; i < 4; i++){
            for (int j =0;j < 4; j++){
                rezult += data[i *4 +j]+" ";
            }
            rezult+="\n";
        }
        return rezult;
    }
}
