package com.brawlers.cata.brawlers2.Component.LowLevel;

import com.brawlers.cata.brawlers2.Component.Utils.Utils;

import java.nio.FloatBuffer;
import java.util.ArrayList;

/**
 * Created by Cata on 9/20/2017.
 */

public class TextureAtlas {
    private int textureCountWidth;
    private int textureCountHeight;

    private Texture atlas;
    private ArrayList<float[]> coords;

    //Make sure the atlas is fully filled
    public TextureAtlas(Texture texture, int textureCountWidth, int textureCountHeight){
        cnt = 0;
        coords = new ArrayList<>();
        this.atlas = texture;
        this.textureCountWidth = textureCountWidth;
        this.textureCountHeight = textureCountHeight;
        float x = 1.0f/textureCountWidth;
        float y = 1.0f/textureCountHeight;

        int cnt = 0;
        for (float i = 0.0f; i < 1.0f; i+= x){
            for (float j = 0.0f; j < 1.0f; j+=y){
                float[] coordsPerImage={i,      j,
                                        i,      (j+y),
                                        (i+x),  (j+y),
                                        (i+x),  j};
                cnt++;
                coords.add(coordsPerImage);
            }
        }
    }

    public void Bind(){
        atlas.Bind();
    }
    public void Unbind(){
        atlas.Unbind();
    }

    private int cnt;
    public FloatBuffer getNextIndex(){
       return Utils.toFloatBuffer(coords.get(cnt));
    }
    public FloatBuffer getImage(int index){
        return Utils.toFloatBuffer(coords.get(index));
    }

}
