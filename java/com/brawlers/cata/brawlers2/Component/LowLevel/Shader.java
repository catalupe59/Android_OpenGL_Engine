package com.brawlers.cata.brawlers2.Component.LowLevel;

import android.opengl.GLES20;

import android.util.Log;
import android.util.Pair;

import com.brawlers.cata.brawlers2.Component.Utils.ShaderUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cata on 9/8/2017.
 */

/*
REDO THIS SHIT
 */

public class Shader {
    private static HashMap<Pair<String,String>,Shader> shaderPool = new HashMap<>();
    public final static int NOTSET = Integer.MIN_VALUE;

    public int shaderID;
    public String vertexCode;
    public String fragmentCode;

    private  int vertexPosition;
    private  int modelSpace ;
    private  int cameraSpace ;
    private  int projectionSpace;
    private  int textureCoordinates ;
    private  int textureSampler;
    private  int color ;

    private Shader(){}

    public static void fillShaderCache(int level){
        Pair<String, String> queryPair = new Pair<>("Unlit.vertex", "Unlit.fragment");
        Shader shader = createShader(queryPair.first, queryPair.second);
        shaderPool.put(queryPair,shader);

        queryPair = new Pair<>("Unlit2.vertex", "Unlit2.fragment");
        shader = createShader(queryPair.first, queryPair.second);
        shaderPool.put(queryPair,shader);
    }

    public static Shader getShader(String vertexLocation ,String fragmentLocation){
        Pair<String, String> queryPair = new Pair<>(vertexLocation, fragmentLocation);
        if (shaderPool.containsKey(queryPair)){
            Log.d("Shader", "Found the key");
            return shaderPool.get(queryPair);
        }else{
            Shader shader = createShader(vertexLocation, fragmentLocation);
            shaderPool.put(queryPair,shader);
            Log.d("Shader", "Created a new shader");
            return shader;
        }

    }


    private static Shader createShader(String vertexLocation, String fragmentLocation){
        Shader shader = new Shader();


        int vertexID = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        int fragmentID = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);

        shader.vertexCode = ShaderUtils.LoadTextFile(vertexLocation);
        shader.fragmentCode =  ShaderUtils.LoadTextFile(fragmentLocation);
        GLES20.glShaderSource(vertexID, shader.vertexCode);
        GLES20.glShaderSource(fragmentID, shader.fragmentCode);


        GLES20.glCompileShader(vertexID);
        int[] compileStatus = new int[1];
        GLES20.glGetShaderiv(vertexID, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] != GLES20.GL_TRUE) {
            Log.d("Shader error vertex", GLES20.glGetShaderInfoLog(vertexID));
        }

        GLES20.glCompileShader(fragmentID);
        compileStatus = new int[1];
        GLES20.glGetShaderiv(fragmentID, GLES20.GL_COMPILE_STATUS, compileStatus, 0);
        if (compileStatus[0] != GLES20.GL_TRUE) {
            Log.d("Shader error fragment", GLES20.glGetShaderInfoLog(fragmentID));
        }

        shader.shaderID = GLES20.glCreateProgram();
        GLES20.glAttachShader(shader.shaderID, vertexID);
        GLES20.glAttachShader(shader.shaderID, fragmentID);
        GLES20.glLinkProgram(shader.shaderID);
        //Check for errors
        int[] linkstatus = new int[1];
        GLES20.glGetProgramiv(shader.shaderID, GLES20.GL_LINK_STATUS, linkstatus, 0);
        if (linkstatus[0] != GLES20.GL_TRUE){
            Log.d("Shader error link",GLES20.glGetProgramInfoLog(shader.shaderID));
        }
        GLES20.glDetachShader(shader.shaderID, vertexID);
        GLES20.glDetachShader(shader.shaderID, fragmentID);
        GLES20.glDeleteShader(vertexID);
        GLES20.glDeleteShader(fragmentID);

        shader.vertexPosition = GLES20.glGetAttribLocation(shader.shaderID, "vertexPosition");
        if (shader.vertexPosition < 0){
            shader.vertexPosition = NOTSET;
        }
        shader.modelSpace = GLES20.glGetUniformLocation(shader.shaderID, "modelSpace");
        if (shader.modelSpace < 0){
            shader.modelSpace = NOTSET;
        }
        shader.cameraSpace = GLES20.glGetUniformLocation(shader.shaderID, "cameraSpace");
        if (shader.cameraSpace < 0){
            shader.cameraSpace = NOTSET;
        }
        shader.projectionSpace = GLES20.glGetUniformLocation(shader.shaderID, "projectionSpace");
        if (shader.projectionSpace < 0){
            shader.projectionSpace = NOTSET;
        }
        shader.textureCoordinates = GLES20.glGetAttribLocation(shader.shaderID, "textureCoordinates");
        if (shader.textureCoordinates < 0){
            shader.textureCoordinates = NOTSET;
        }
        shader.textureSampler = GLES20.glGetUniformLocation(shader.shaderID, "textureSampler");
        if (shader.textureSampler < 0){
            shader.textureSampler = NOTSET;
        }
        shader.color = GLES20.glGetUniformLocation(shader.shaderID, "color");
        if (shader.color < 0){
            shader.color = NOTSET;
        }

        Pair<Pair<String, String>, Shader> pair = new Pair<>(new Pair<>(shader.vertexCode,shader.fragmentCode),shader);


        return shader;
    }
    @Override
    public String toString(){
        return "Vertex code of "+ vertexCode + "\n fragment code of "+ fragmentCode;
    }
    public int getShaderID(){
        return shaderID;
    }

    @Override
    public boolean equals(Object obj) {
        return ((Shader)obj).shaderID == shaderID;
    }


    public int getVertexPositionLocation(){
        return vertexPosition;
    }
    public int getModelSpaceLocation(){
        return modelSpace;
    }
    public int getCameraSpaceLocation(){
        return cameraSpace;
    }
    public int getProjectionSpaceLocation(){
        return projectionSpace;
    }
    public int getTextureCoordinatesLocation(){
        return textureCoordinates;
    }
    public int getTextureSamplerLocation(){
        return textureSampler;
    }
    public int getColorLocation(){
        return color;
    }
}
