package com.brawlers.cata.brawlers2.Component.Utils;

import android.util.Pair;

import com.brawlers.cata.brawlers2.Main;

/**
 * Created by Cata on 9/17/2017.
 */

public class Screen {
    public static Pair<Integer,Integer> screenSize(){
        return Main.thisMain.getScreenSize();
    }
    public static Pair<Float,Float> pixelToGL(Pair<Integer,Integer> coord){
        //Normalize interval from [-1, 1] to [0 , 2]
        float normX = coord.first + 1.0f;
        float normY = coord.second + 1.0f;
        //How much % counts the coord in the interval
        float x = (normX * 100.0f) / 2.0f;
        float y = (normY * 100.0f) / 2.0f;

        return new Pair<>((x/100.0f)*screenSize().first,((y/100.0f)*screenSize().second));
    }
    //TODO invert
}
