package com.brawlers.cata.brawlers2.Component.Utils;

import android.util.Log;

/**
 * Created by Cata on 9/17/2017.
 */

public class Stats {
    private static int textureCount;

    public static void IncreaseTextureCount(){
        textureCount++;
    }

    public static void LogTextureCount(){
        Log.d("Stats: ","Texture count = "+textureCount);
    }
}
