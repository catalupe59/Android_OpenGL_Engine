package com.brawlers.cata.brawlers2.Component.Utils;

import android.util.Pair;

import com.brawlers.cata.brawlers2.Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Cata on 9/16/2017.
 */

public class ShaderUtils {
    public static String LoadTextFile(String location) {
        try {
            InputStream input = Main.getAssetManager().open(location);
            BufferedReader buffrdr = new BufferedReader(new InputStreamReader(input));
            StringBuffer stringBuffer = new StringBuffer();
            String line;
            while ((line = buffrdr.readLine()) != null){
                stringBuffer.append(line);
                stringBuffer.append("\n");
            }
            input.close();
            return stringBuffer.toString();
        } catch(IOException e){
            return "";
        }
    }
    public static Pair<ArrayList<String>, ArrayList<String>> getVariableList(String shaderCode){
        ArrayList<String> attributesList = new ArrayList<String>();
        ArrayList<String> uniformsList = new ArrayList<String>();
        String[] codeList = shaderCode.split("\n");
        for (String line : codeList){
            if (line.contains("attribute")){
                attributesList.add(getVariable(line));
            }else if (line.contains("uniform")){
                uniformsList.add(getVariable(line));
            }
        }
        return new Pair<>(attributesList, uniformsList);
    }

    private static String getVariable(String line){
        return line.split(" ")[2];
    }
}
