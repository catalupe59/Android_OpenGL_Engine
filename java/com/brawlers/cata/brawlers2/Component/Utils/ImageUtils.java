package com.brawlers.cata.brawlers2.Component.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.brawlers.cata.brawlers2.Main;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Cata on 9/16/2017.
 */

public class ImageUtils {
    public static Bitmap LoadImage(String location){
        Bitmap image = null;
        try{
            InputStream input = Main.getAssetManager().open(location);
            image = BitmapFactory.decodeStream(input);
        }catch(IOException e){

        }
        return image;
    }
}
