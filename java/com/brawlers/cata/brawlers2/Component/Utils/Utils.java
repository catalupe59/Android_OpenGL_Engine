package com.brawlers.cata.brawlers2.Component.Utils;

import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Vector3f;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;


/**
 * Created by Cata on 9/8/2017.
 */

public class Utils {
    public static FloatBuffer toFloatBuffer(float[] buffer){
        ByteBuffer bb = ByteBuffer.allocateDirect(4 * buffer.length).order(ByteOrder.nativeOrder());
        FloatBuffer fb = bb.asFloatBuffer();
        fb.put(buffer);
        fb.position(0);
        return fb;
    }

    public static FloatBuffer toFloatBuffer(ArrayList<Vector3f> vectors){
        float [] buffer = new float [vectors.size() * 3];
        for (int i = 0 ; i < vectors.size(); i+=3){
            buffer[i] = vectors.get(i).x;
            buffer[i+1] = vectors.get(i).y;
            buffer[i+2] = vectors.get(i).z;
        }
        return toFloatBuffer(buffer);
    }

    public static FloatBuffer toFloatBuffer(Vector3f vec){
        float [] vBuffer = new float[3];
        vBuffer[0] = vec.x;
        vBuffer[1] = vec.y;
        vBuffer[2] = vec.z;
        return toFloatBuffer(vBuffer);
    }
    public static ShortBuffer toShortBuffer(short[] buffer){
        ByteBuffer bb = ByteBuffer.allocateDirect(2 * buffer.length).order(ByteOrder.nativeOrder());
        ShortBuffer ib = bb.asShortBuffer();
        ib.put(buffer);
        ib.position(0);
        return ib;
    }
    public static float toOneDigit(int number){
        float x = number;
        while (x/10.0f > 9){
            x /= 10.0f;

        }
        return x;
    }
}
