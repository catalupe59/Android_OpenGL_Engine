package com.brawlers.cata.brawlers2.SerializationModule;

import com.brawlers.cata.brawlers2.Level.Level;
import com.brawlers.cata.brawlers2.Main;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by Cata on 9/24/2017.
 */

//TODO FIX THIS
    //DEPRECATED
public class Serializer {
    public static void writeLevel(String assetName, Level object) {
        try {
            FileOutputStream fostream = Main.thisMain.openFileOutput(assetName, 1);

            ObjectOutputStream obj = new ObjectOutputStream(fostream);
            obj.writeObject(object);
            obj.close();
        }catch(java.io.IOException e) {

        }
    }

    public static Level readLevel(String name){
        try {
            FileInputStream finstream = Main.thisMain.openFileInput(name);
            ObjectInputStream in = new ObjectInputStream(finstream);
            Level lvl = (Level)in.readObject();
            return lvl;
        }catch(java.io.IOException e){
            e.printStackTrace();
        }catch (java.lang.ClassNotFoundException e){
            e.printStackTrace();
        }
        return null;
    }
}
