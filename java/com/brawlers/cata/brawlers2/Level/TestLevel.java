package com.brawlers.cata.brawlers2.Level;

import android.opengl.Matrix;

import com.brawlers.cata.brawlers2.Component.LowLevel.Shader;
import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Vector3f;
import com.brawlers.cata.brawlers2.Component.LowLevel.Texture;
import com.brawlers.cata.brawlers2.MdlLoader.MdlParser;
import com.brawlers.cata.brawlers2.Program.Clock;
import com.brawlers.cata.brawlers2.obj.obj.Camera;
import com.brawlers.cata.brawlers2.obj.GameObject;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.Material;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.Mesh;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.MeshRenderer;
import com.brawlers.cata.brawlers2.obj.obj.Transform;
import com.brawlers.cata.brawlers2.obj.obj.eComponentType;

import java.util.ArrayList;

/**
 * Created by Cata on 9/17/2017.
 */

public class TestLevel extends Level{
    ArrayList<GameObject> objects;
    GameObject camera;

    public TestLevel(){
        camera = new GameObject(new Transform(
                new Vector3f(0.0f, 0.0f, -2.0f),
                new Vector3f(1,1,1),
                0.0f));

        objects = new ArrayList<>();

        for (int i =0 ; i < 1; i++){
            GameObject gameObject = new GameObject();
            gameObject.addComponent(new Transform(new Vector3f(0, 0.0f, -5.0f),new Vector3f(0.5f,0.5f,0.5f),0.0f));
            gameObject.addComponent(new Mesh(MdlParser.ObjToMesh("cube1x1.obj"),
                                    new MeshRenderer(new Material(Shader.getShader("Unlit2.vertex","Unlit2.fragment"),new Texture("monkey_ao.png")))));
            objects.add(gameObject);
        }
    }

    public void Start(){
    }
    public void Render(){
        for (int i =0; i < objects.size(); i++){
            objects.get(i).render();
        }
    }

    public void Update() {

    }
    public void Reload(int width, int height){
        Camera.Main = new Camera(width/100, height/100);
        Camera.Main.setTransform((Transform)camera.getComponent(eComponentType.Transform));
    }
}
