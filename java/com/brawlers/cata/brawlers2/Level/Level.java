package com.brawlers.cata.brawlers2.Level;

import android.util.Log;

import com.brawlers.cata.brawlers2.Component.Utils.Screen;
import com.brawlers.cata.brawlers2.Component.Utils.Stats;

import java.io.Serializable;

/**
 * Created by Cata on 9/16/2017.
 */

/*
TODO: FIX SERIALIZATION
 */
public abstract class Level implements java.io.Serializable  {

    public abstract void Start();

    private void Log(){
        Log.d("Screen Size: ", Screen.screenSize().first+" "+Screen.screenSize().second);
        Stats.LogTextureCount();
    }

    public abstract void Reload(int width, int height);
    public abstract void Update();
    public abstract void Render();
}
