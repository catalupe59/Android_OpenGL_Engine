package com.brawlers.cata.brawlers2.Viewer;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import com.brawlers.cata.brawlers2.Renderer.EditorRenderer;
import com.brawlers.cata.brawlers2.Renderer.GLRenderer;

/**
 * Created by Cata on 9/15/2017.
 */

public class GLEditor extends GLViewer {

    public GLEditor(Context context){
        super(context);
        setEGLContextClientVersion(2);
        setEGLConfigChooser(8,8,8,8,16,0);
        mRenderer = new EditorRenderer();
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

    }

    public  boolean onTouchEvent(MotionEvent e){
        return false;
    }
}
