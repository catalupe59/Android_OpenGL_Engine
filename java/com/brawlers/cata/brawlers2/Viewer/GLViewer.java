package com.brawlers.cata.brawlers2.Viewer;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

import com.brawlers.cata.brawlers2.Renderer.GLRenderer;

/**
 * Created by Cata on 9/8/2017.
 */

public abstract class GLViewer extends GLSurfaceView {
    protected GLRenderer mRenderer;

    protected GLViewer(Context context) {
        super(context);
    }

    @Override
    public abstract boolean onTouchEvent(MotionEvent e);

}
