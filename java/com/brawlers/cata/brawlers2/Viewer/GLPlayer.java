package com.brawlers.cata.brawlers2.Viewer;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;

import com.brawlers.cata.brawlers2.Component.Utils.Input;
import com.brawlers.cata.brawlers2.Renderer.GLRenderer;
import com.brawlers.cata.brawlers2.Renderer.PlayerRenderer;

/**
 * Created by Cata on 9/15/2017.
 */

public class GLPlayer extends GLViewer {

    public GLPlayer(Context context){
        super(context);
        setEGLContextClientVersion(2);
        setEGLConfigChooser(8,8,8,8,16,0);
        mRenderer = new PlayerRenderer();
        setRenderer(mRenderer);
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }

    public  boolean onTouchEvent(MotionEvent e){
        switch (e.getAction()){
            case MotionEvent.ACTION_DOWN:
                Input.pressed = true;
                break;
            case MotionEvent.ACTION_UP:
                Input.pressed = false;
                break;
        }

        return true;
    }
}
