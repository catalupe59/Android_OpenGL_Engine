package com.brawlers.cata.brawlers2.Program;

import android.util.Log;

import java.util.concurrent.Callable;

/**
 * Created by Cata on 9/16/2017.
 */

public class Coroutine {
    private Clock clock;
    private double seconds;

    public Coroutine(double seconds) {
        this.seconds = seconds;
        clock = new Clock();
    }
    public void run(Callable<Void> function){
        try{
            if ( clock != null && clock.elapsedTime() <= seconds) {
                Log.d("Elapsed time ", clock.elapsedTime()+"");
                function.call();
            } else {
                clock = null;
            }
        }catch(java.lang.Exception e){
            e.printStackTrace();
        }
    }
}
