package com.brawlers.cata.brawlers2.Program;

import com.brawlers.cata.brawlers2.Component.LowLevel.Shader;
import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Vector3f;
import com.brawlers.cata.brawlers2.Level.Level;
import com.brawlers.cata.brawlers2.Level.TestLevel;
import com.brawlers.cata.brawlers2.obj.GameObject;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.Material;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.Mesh;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.MeshFilter;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.MeshRenderer;
import com.brawlers.cata.brawlers2.obj.obj.Transform;
import com.brawlers.cata.brawlers2.obj.obj.eComponentType;

/**
 * Created by Cata on 9/30/2017.
 */

public class Loader {
    public boolean isDone;
    public Level loadedInstance;
    private GameObject loadVisual;

    private Thread loader;

    public Loader(int levelCnt){
        loader = new Thread(){
            @Override
            public void run() {
                loadedInstance = new TestLevel();
                isDone = true;
            }
        };
        loader.start();

        loadVisual = new GameObject();
        loadVisual.addComponent(new Transform(new Vector3f(0,0,-5), new Vector3f(1,1,1),0.0f));
        loadVisual.addComponent(new Mesh(new MeshFilter(), new MeshRenderer(new Material(Shader.getShader("Unlit.vertex","Unlit.fragment")))));
    }

    public void update(){
        ((Transform)loadVisual.getComponent(eComponentType.Transform)).rotate(20*(float)Clock.elapsedGlobalTime());
    }
    public void render(){
        loadVisual.render();
    }
    public Level getLoadedInstance(){
        return loadedInstance;
    }


}
