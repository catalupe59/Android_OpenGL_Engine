package com.brawlers.cata.brawlers2.Program;

import android.opengl.GLES20;

import com.brawlers.cata.brawlers2.Component.Utils.FPSCounter;
import com.brawlers.cata.brawlers2.Level.Level;
import com.brawlers.cata.brawlers2.Level.TestLevel;

/**
 * Created by Cata on 9/15/2017.
 */

public class Program  {

    private LevelManager levelManager;
    private final float FPS = 60.0f;
    private Clock clock = new Clock();
    private FPSCounter fpsCounter = new FPSCounter();

    private static Program editorInstance;
    private static Program playerInstance;

    public static Program loadEditor(){
        playerInstance = null;
        if (editorInstance == null) {
            editorInstance = new Program();
        }
        return editorInstance;
    }
    public static Program loadPlayer(){
        editorInstance = null;
        if (playerInstance == null){
            playerInstance = new Program();
        }
        return playerInstance;
    }

    private Program(){
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glDepthFunc(GLES20.GL_LEQUAL);
        GLES20.glDepthMask(true);
        GLES20.glCullFace(GLES20.GL_BACK);

        levelManager = LevelManager.getInstance();
        levelManager.loadLevel(0);
    }

    public void Start(){
        Clock.Init();
        levelManager.start();

    }

    private Runnable render;
    private Runnable update;
    public void Loop(){
        clock.Refresh();

        Update();
        Render();
//        update.run();
//        render.run();

        fpsCounter.logFrame();
    }
    public void Update(){
        levelManager.update();

    }
    public void Render(){
        levelManager.render();
    }
    public void Reload(int width, int height){
        levelManager.reload(width, height);
    }
}
