package com.brawlers.cata.brawlers2.Program;

/**
 * Created by Cata on 9/16/2017.
 */

public class Clock {
    public static long globalStartTime;
    public static void Init(){
        globalStartTime = System.currentTimeMillis();
    }
    public static double elapsedGlobalTime(){
        long tEnd = System.currentTimeMillis();
        long delta = tEnd - globalStartTime;
        double elapsed = delta/1000.0;
        return elapsed;
    }

    public long startTime;
    public Clock(){
        startTime = System.currentTimeMillis();
    }
    public double elapsedTime(){
        long tEnd = System.currentTimeMillis();
        long delta = tEnd - startTime;
        double elapsed = delta/1000.0;
        return elapsed;
    }
    public void Refresh(){
        startTime = System.currentTimeMillis();
    }

}
