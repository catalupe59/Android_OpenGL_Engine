package com.brawlers.cata.brawlers2.Program;

import android.util.Log;

import com.brawlers.cata.brawlers2.Component.LowLevel.Shader;
import com.brawlers.cata.brawlers2.Component.LowLevel.Texture;
import com.brawlers.cata.brawlers2.Level.Level;
import com.brawlers.cata.brawlers2.obj.GameObject;
import com.brawlers.cata.brawlers2.obj.obj.Camera;
import com.brawlers.cata.brawlers2.obj.obj.Transform;
import com.brawlers.cata.brawlers2.obj.obj.eComponentType;

import org.w3c.dom.Text;

/**
 * Created by Cata on 9/30/2017.
 */

public class LevelManager {
    private LevelManager(){}
    private Loader loader;
    private Level activeLevel;
    private static LevelManager instance;
    public static LevelManager getInstance(){
        if (instance == null){
            instance = new LevelManager();
        }
        return instance;
    }

    public void loadLevel(int level){
        Shader.fillShaderCache(level);
        Texture.fillTextureCache(level);

        loader = new Loader(level);
    }

    public void start(){
    }
    public void reload(int width, int height){
        GameObject camera = new GameObject();
        camera.addComponent(new Transform());
        Camera.Main = new Camera(width/100, height/100);
        Camera.Main.setTransform((Transform)camera.getComponent(eComponentType.Transform));
    }
    public void update(){
        if (activeLevel != null){
            activeLevel.Update();
            return;
        }
        if (!loader.isDone) {
            loader.update();
        }else{
            activeLevel = loader.getLoadedInstance();
        }

    }
    public void render(){
        if (activeLevel != null){
            activeLevel.Render();
            return;
        }
        if (!loader.isDone){
            loader.render();
        }

    }

}
