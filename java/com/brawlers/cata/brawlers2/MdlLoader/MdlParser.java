package com.brawlers.cata.brawlers2.MdlLoader;

import android.util.Log;

import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Vector2f;
import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Vector3f;
import com.brawlers.cata.brawlers2.Component.Utils.ShaderUtils;
import com.brawlers.cata.brawlers2.obj.Obj;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.MeshFilter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Cata on 9/20/2017.
 */

public class MdlParser {
    private static HashMap<String, MeshFilter> modelPool = new HashMap<>();


    //Proces strings
    public static MeshFilter ObjToMesh(String location) {
        if (modelPool.containsKey(location)) {
            return modelPool.get(location);
        } else {
            MeshFilter mesh = objectParse(location);
            modelPool.put(location, mesh);
            return mesh;
        }
    }
    private static MeshFilter objectParse(String location){
        ArrayList<Vector3f>parsedVertex = new ArrayList<>();
        ArrayList<Vector2f>parsedTexture = new ArrayList<>();
        ArrayList<Vector3f>parsedNormals = new ArrayList<>();

        ArrayList<Short>vertexOrder = new ArrayList<>();
        ArrayList<Short>textureOrder = new ArrayList<>();
        ArrayList<Short>normalOrder = new ArrayList<>();

        String code = ShaderUtils.LoadTextFile(location);
        String[] lines = code.split("\n");


        Scanner scanner;
        for (String line : lines){
            //If it's a comentary drop the line
            if (line.charAt(0) == '#'){
                continue;
            }else {
                scanner = new Scanner(line);
                if (line.charAt(0) == 'v' && line.charAt(1) == ' '){
                    while (!scanner.hasNextFloat()) {
                        scanner.next();
                    }
                    float x=scanner.nextFloat();
                    float y = scanner.nextFloat();
                    float z = scanner.nextFloat();
                    parsedVertex.add(new Vector3f(x,y,z));
                }else if (line.charAt(0) == 'v' && line.charAt(1)=='t'){
                    while(!scanner.hasNextFloat()){
                        scanner.next();
                    }
                    float x = scanner.nextFloat();
                    float y = scanner.nextFloat();
                    parsedTexture.add(new Vector2f(x,y));
                }else if (line.charAt(0) == 'v' && line.charAt(1)=='n'){
                    while(!scanner.hasNextFloat()){
                        scanner.next();
                    }
                    float x = scanner.nextFloat();
                    float y=  scanner.nextFloat();
                    float z = scanner.nextFloat();
                    parsedNormals.add( new Vector3f(x,y,z));
                }else if (line.charAt(0)=='f'){
                    String replcedLine = line.replace('/',' ');
                    scanner.close();
                    scanner = new Scanner(replcedLine);
                    {
                        for (int i = 0; i < 3 ; i++) {
                            while (!scanner.hasNextShort()) {
                                scanner.next();
                            }
                            vertexOrder.add((short)(scanner.nextShort()-1));
                            textureOrder.add((short)(scanner.nextShort()-1));
                            normalOrder.add((short)(scanner.nextShort()-1));
                        }
                    }
                }else{
                    scanner.close();
                    continue;
                }
                scanner.close();
            }
        }


        return cachedToMesh(parsedVertex, parsedNormals, parsedTexture, vertexOrder, normalOrder, textureOrder);


    }

    private static MeshFilter cachedToMesh(ArrayList<Vector3f>parsedVertex , ArrayList<Vector3f>parsedNormals, ArrayList<Vector2f>parsedTexture,
                                           ArrayList<Short>vertexOrder, ArrayList<Short>normalsOrder, ArrayList<Short>textureOrder){
        //TODO SET FOR INDICES
        //Set vertex data

        ArrayList<Vector3f> verticesBuffer = new ArrayList<>();
        for (int i =0; i < vertexOrder.size(); i++){
            verticesBuffer.add(parsedVertex.get(vertexOrder.get(i)));
        }
        ArrayList<Vector2f> textureCoordinatesBuffer = new ArrayList<>();
        for (int i =0; i < textureOrder.size(); i++){
            textureCoordinatesBuffer.add(parsedTexture.get(textureOrder.get(i)));
        }
        ArrayList<Vector3f> normalsBuffer = new ArrayList<>();
        for (int i =0; i < normalsOrder.size(); i++){
            normalsBuffer.add(parsedNormals.get(normalsOrder.get(i)));
        }

        return filter(verticesBuffer, normalsBuffer,textureCoordinatesBuffer);
    }


    /*
    VBO INDEXER
     */

    private static MeshFilter filter(ArrayList<Vector3f> vertices , ArrayList<Vector3f> normals, ArrayList<Vector2f> texture){
        ArrayList<Vector3f> vertexElements = new ArrayList<>();
        ArrayList<Vector3f> normalsElements = new ArrayList<>();
        ArrayList<Vector2f> textureElements = new ArrayList<>();
        ArrayList<Short> elements = new ArrayList<>();
        /*
        vertices.size() == normals.size() == texture.size()
         */
        short cnt = 0;
        for (int i= 0; i < vertices.size(); i++){
            int found = findSimilar(vertices.get(i), normals.get(i), texture.get(i), vertexElements, normalsElements, textureElements);
//            int found = -1;
            if (found == -1){
                //not found add new element
                vertexElements.add(vertices.get(i));
                textureElements.add(texture.get(i));
                normalsElements.add(normals.get(i));
                elements.add(cnt);
                cnt++;
            } else {
                //found
                elements.add((short)found);
            }
        }

        float[] verts = new float[vertexElements.size()*3];
        float [] texts = new float[textureElements.size()*2];
        float [] norms = new float[normalsElements.size()*3];
        short[] elems = new short[elements.size()];

        for (int i =0, j =0; i < verts.length; i+=3, j++){
            verts[i] = vertexElements.get(j).x;
            verts[i+1] = vertexElements.get(j).y;
            verts[i+2] = vertexElements.get(j).z;
        }

        for (int i =0, j = 0; i<norms.length; i+=3, j++ ){
            norms[i] = normalsElements.get(j).x;
            norms[i+1] = normalsElements.get(j).y;
            norms[i+2] = normalsElements.get(j).z;
        }

        for (int i =0, j = 0; i < texts.length; i+=2, j++){
            texts[i] = textureElements.get(j).x;
            texts[i+1] = textureElements.get(j).y;
        }

        for (int i =0; i < elems.length; i++){
            elems[i] = elements.get(i);
        }

        return new MeshFilter(verts, elems, texts);
    }

    /*
    Naive linear approach
    Testing purposes
     */
    private static int findSimilar(Vector3f vert, Vector3f normal, Vector2f texture, ArrayList<Vector3f> outVertices, ArrayList<Vector3f> outNormals, ArrayList<Vector2f> outTexture){
        int i =0;
        for (i = 0; i < outVertices.size(); i++) {
            if (isNear(vert.x, outVertices.get(i).x) &&
                    isNear(vert.y, outVertices.get(i).y) &&
                    isNear(vert.z, outVertices.get(i).z) &&
                    isNear(normal.x, outNormals.get(i).x) &&
                    isNear(normal.y, outNormals.get(i).y) &&
                    isNear(normal.z, outNormals.get(i).z) &&
                    isNear(texture.x, outTexture.get(i).x)  &&
                    isNear(texture.y, outTexture.get(i).y)){
                        return i;
            }
        }
        return -1;

    }
    private static boolean isNear(float v1, float v2){
        return Math.abs(v1-v2) < 0.01f;
    }

}
