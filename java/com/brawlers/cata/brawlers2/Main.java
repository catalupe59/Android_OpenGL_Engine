package com.brawlers.cata.brawlers2;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;

import com.brawlers.cata.brawlers2.Level.TestLevel;
import com.brawlers.cata.brawlers2.SerializationModule.Serializer;
import com.brawlers.cata.brawlers2.Viewer.GLEditor;
import com.brawlers.cata.brawlers2.Viewer.GLPlayer;
import com.brawlers.cata.brawlers2.Viewer.GLViewer;

import java.io.Serializable;

public class Main extends Activity implements View.OnClickListener {

    public static Main thisMain;
    private static AssetManager assetManager;
    private GLViewer glGLViewer;
    private Button editor;
    private Button player;
    private Button serializer;
    private Button deserializer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        assetManager = this.getAssets();

        editor = (Button)findViewById(R.id.Button_Editor);
        player = (Button)findViewById(R.id.Button_Player);
        serializer = (Button)findViewById(R.id.Button_Serialize);
        deserializer = (Button)findViewById(R.id.Button_Deserializer);

        player.setOnClickListener(this);
        editor.setOnClickListener(this);
        serializer.setOnClickListener(this);
        deserializer.setOnClickListener(this);

        thisMain = this;

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.Button_Editor:
                glGLViewer = new GLEditor(this);
                setContentView(glGLViewer);
                break;
            case R.id.Button_Player:
                glGLViewer = new GLPlayer(this);
                setContentView(glGLViewer);
                break;
            case R.id.Button_Serialize:
                Serializer.writeLevel("Test",new TestLevel());
                break;
            case R.id.Button_Deserializer:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setContentView(R.layout.activity_main);
    }
    public static AssetManager getAssetManager(){
        return assetManager;
    }

    public Pair<Integer,Integer> getScreenSize(){
        return new Pair<>(glGLViewer.getWidth(), glGLViewer.getHeight());
    }
}
