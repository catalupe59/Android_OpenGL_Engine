package com.brawlers.cata.brawlers2.Renderer;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.util.Log;

import com.brawlers.cata.brawlers2.Program.Program;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

/**
 * Created by Cata on 9/8/2017.
 */

public abstract class GLRenderer implements GLSurfaceView.Renderer {
    protected Program program;
    public abstract void onSurfaceCreated(GL10 unused, EGLConfig config);
    public abstract void onDrawFrame(GL10 unused);
    public abstract void onSurfaceChanged(GL10 unused, int width, int height);

//        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//
//        program = Program.getInstance();
//        program.Start();
//        GLES20.glClearColor(0,0,0,1);
//        fps = new FPSCounter();

//        fps.logFrame();
//        clock+=0.01f;
//        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT );
//        GLES20.glClearColor(0,0,(float)Math.sin(clock),0.5f);
//        program.Run();


//        program.Reload(width,height);

}
