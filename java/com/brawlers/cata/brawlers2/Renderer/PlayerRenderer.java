package com.brawlers.cata.brawlers2.Renderer;

import android.opengl.GLES20;

import com.brawlers.cata.brawlers2.Program.Program;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Cata on 9/15/2017.
 */

public class PlayerRenderer extends GLRenderer {
    public  void onSurfaceCreated(GL10 unused, EGLConfig config){
        program = Program.loadPlayer();
        program.Start();
        GLES20.glClearColor(0,1,0,1);
    }
    public  void onDrawFrame(GL10 unused){
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glClearColor(0,0,0.1f,0.5f);
        program.Loop();
    }
    public  void onSurfaceChanged(GL10 unused, int width, int height) {
        program.Reload(width, height);
    }
}
