package com.brawlers.cata.brawlers2.obj.obj.Mesh;

import android.opengl.GLES20;

import com.brawlers.cata.brawlers2.Component.LowLevel.Texture;
import com.brawlers.cata.brawlers2.obj.obj.Camera;
import com.brawlers.cata.brawlers2.Component.LowLevel.Shader;
import com.brawlers.cata.brawlers2.Component.LowLevel.TextureAtlas;
import com.brawlers.cata.brawlers2.Component.Utils.Utils;
import com.brawlers.cata.brawlers2.obj.obj.Transform;
import static android.opengl.GLES20.glUseProgram;
/**
 * Created by Cata on 9/28/2017.
 */

public class Material {

    public Material(Shader shader){
        this.shader = shader;
    }

    public Material(Shader shader, Texture texture){
        this(shader);
        this.texture = texture;
    }

    private Shader shader;
    private Texture texture;


    public void render(RenderType type, MeshFilter meshFilter, Camera camera, Transform transform) {
        glUseProgram(shader.getShaderID());


        if(texture!= null) {
            texture.Bind();
        }

        int vertexPosition = shader.getVertexPositionLocation();
        int modelSpace = shader.getModelSpaceLocation();
        int cameraSpace = shader.getCameraSpaceLocation();
        int projectionSpace = shader.getProjectionSpaceLocation();
        int textureCoordinates = shader.getTextureCoordinatesLocation();
        int textureSampler = shader.getTextureSamplerLocation();
        int color = shader.getColorLocation();


        GLES20.glEnableVertexAttribArray(vertexPosition);

        GLES20.glVertexAttribPointer(vertexPosition, 3, GLES20.GL_FLOAT, false, 12, meshFilter.getVertexBuffer());

        if (modelSpace != Shader.NOTSET) {
            GLES20.glUniformMatrix4fv(modelSpace, 1, false, Utils.toFloatBuffer(transform.getMVP().Transpose().getData()));
        }

        if (cameraSpace != Shader.NOTSET) {
            GLES20.glUniformMatrix4fv(cameraSpace, 1, false, Utils.toFloatBuffer(camera.getTransform().getMVP().Transpose().getData()));
        }

        if (projectionSpace != Shader.NOTSET) {
            GLES20.glUniformMatrix4fv(projectionSpace, 1, false, Utils.toFloatBuffer(camera.getProjectionMatrix().getData()));
        }

        if (textureCoordinates != Shader.NOTSET) {
            GLES20.glEnableVertexAttribArray(textureCoordinates);
            GLES20.glVertexAttribPointer(textureCoordinates, 2, GLES20.GL_FLOAT, false, 8, meshFilter.getTextureIndexBuffer());
        }
        if (textureSampler != Shader.NOTSET){
            GLES20.glUniform1i(textureSampler, 0);
        }

        GLES20.glDrawElements(type.getType(), meshFilter.getElementsSize() , GLES20.GL_UNSIGNED_SHORT ,meshFilter.getElementsBuffer());
//        GLES20.glDrawArrays(type.getType(), 0, meshFilter.getVertexSize()/3);

        if (textureCoordinates != Shader.NOTSET) {
            GLES20.glDisableVertexAttribArray(textureCoordinates);
        }
        GLES20.glDisableVertexAttribArray(vertexPosition);

        if(texture != null) {
            texture.Unbind();
        }

        glUseProgram(0);
    }
}
