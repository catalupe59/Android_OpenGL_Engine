package com.brawlers.cata.brawlers2.obj.obj.Mesh;

import com.brawlers.cata.brawlers2.obj.obj.Camera;
import com.brawlers.cata.brawlers2.Component.LowLevel.Shader;
import com.brawlers.cata.brawlers2.obj.obj.Transform;
import com.brawlers.cata.brawlers2.obj.obj.eComponentType;
import com.brawlers.cata.brawlers2.obj.obj.iComponent;

/**
 * Created by Cata on 9/28/2017.
 */

public class Mesh implements iComponent {
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    public Mesh(MeshFilter meshFilter, MeshRenderer meshRenderer){
        this.meshRenderer = meshRenderer;
        this.meshFilter = meshFilter;
    }

    public Mesh(){
        this(new MeshFilter(),new MeshRenderer(new Material( Shader.getShader("Unlit.vertex","Unlit.fragment"))));
    }

    public void render(RenderType type, Camera camera, Transform transform){
        meshRenderer.render(type,meshFilter, camera, transform);
    }

    @Override
    public eComponentType getComponent() {
        return eComponentType.Mesh;
    }
}
