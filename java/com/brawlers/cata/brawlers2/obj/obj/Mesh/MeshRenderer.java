package com.brawlers.cata.brawlers2.obj.obj.Mesh;

import com.brawlers.cata.brawlers2.obj.obj.Camera;
import com.brawlers.cata.brawlers2.obj.obj.Transform;

/**
 * Created by Cata on 9/28/2017.
 */

public class MeshRenderer {
    public Material material;

    public MeshRenderer(Material material){
        this.material = material;
    }

    public void render(RenderType type, MeshFilter meshFilter, Camera camera, Transform transform){
        material.render( type,meshFilter, camera, transform );
    }
}
