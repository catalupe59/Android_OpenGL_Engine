package com.brawlers.cata.brawlers2.obj.obj.Mesh;

import com.brawlers.cata.brawlers2.Component.Utils.Utils;

import java.lang.reflect.Array;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Cata on 9/28/2017.
 */

public class MeshFilter {
    private final FloatBuffer vertexArrayBuffer;
    private final ShortBuffer elementsBuffer;
    private final FloatBuffer textureIndexBuffer;

    private final int textureSize;
    private final int elementsSize;
    private final int vertexSize;

    public MeshFilter(){
        this( new float[] {
                -1,  1f, 0.0f,   // top left
                -1f, -1f, 0.0f,   // bottom left
                1f, -1f, 0.0f,   // bottom right
                1f,  1f, 0.0f}, //top right
                new short[]{
                0,1,2,
                0,2,3},
                new float[]{
                0.0f, 0.0f,
                0.0f, 1.0f,
                1.0f, 1.0f,
                1.0f, 0.0f
        });
    }

    public MeshFilter(float [] vertices, short[] elements, float[] coords){
        vertexArrayBuffer = Utils.toFloatBuffer(vertices);
        elementsBuffer = Utils.toShortBuffer(elements);
        textureIndexBuffer = Utils.toFloatBuffer(coords);

        elementsSize = elements.length;
        vertexSize = vertices.length;
        textureSize = coords.length;
    }


    public FloatBuffer getVertexBuffer(){
        return vertexArrayBuffer;
    }
    public ShortBuffer getElementsBuffer(){
        return elementsBuffer;
    }
    public FloatBuffer getTextureIndexBuffer(){return textureIndexBuffer;}
    public int getElementsSize(){
        return elementsSize;
    }
    public int getVertexSize(){
        return vertexSize;
    }
    public int getTextureSize(){return  textureSize;}
}
