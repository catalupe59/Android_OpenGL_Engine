package com.brawlers.cata.brawlers2.obj.obj.Mesh;

import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

/**
 * Created by Cata on 9/28/2017.
 */

public enum RenderType {
    TRIANGLES(0),
    LINES(1),
    POINTS(2);

    private final int index;
    private RenderType(int index){
        this.index = index;
    }
    public int getType(){
        if (this.index == 0){
            return GLES20.GL_TRIANGLES;
        }
        if (this.index == 1) {
            return GLES20.GL_LINES;
        }
        if (this.index == 2){
            return GLES20.GL_POINTS;
        }
        return GLES20.GL_TRIANGLES;
    }
}
