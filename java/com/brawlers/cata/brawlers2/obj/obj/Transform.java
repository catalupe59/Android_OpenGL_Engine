package com.brawlers.cata.brawlers2.obj.obj;

import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Mat4f;
import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Vector3f;
import com.brawlers.cata.brawlers2.obj.GameObject;

import java.util.ArrayList;

/**
 * Created by Cata on 9/28/2017.
 */

public class Transform implements iComponent {
    private boolean isStatic;
    private ArrayList<Transform> children;

    private Vector3f position;
    private Vector3f scale;
//    Change angle with rotation
    float angle;
//    private Vector3f rotation;

    Mat4f objectMVP;
    Mat4f translateMatrix;
    Mat4f scaleMatrix;
    Mat4f rotationMatrix;

    public Transform(){
        this(new Vector3f(), new Vector3f(1.0f,1.0f,1.0f),0.0f);
    }
    public Transform(Vector3f position, Vector3f scale, float angle){
        children = new ArrayList<>();
        this.position = position;
        this.scale = scale;
        this.angle = angle;

        this.translateMatrix = Mat4f.TranslateSt(position);
        this.scaleMatrix = Mat4f.ScaleSt(scale);
        this.rotationMatrix = Mat4f.RotateSt(angle);

        Mat4f temp = Mat4f.Product(translateMatrix, scaleMatrix);
        objectMVP = Mat4f.Product(temp,rotationMatrix);
    }

    public void translate(Vector3f newPosition){
        if (!isStatic) {
            translateMatrix = Mat4f.TranslateSt(newPosition);

            Mat4f temp = Mat4f.Product(translateMatrix, scaleMatrix);
            objectMVP = Mat4f.Product(temp, rotationMatrix);

            for(Transform child : children){
                child.translate(newPosition);
            }
        }
    }

    public void scale(Vector3f newScale){
        if (!isStatic) {
            scaleMatrix = Mat4f.ScaleSt(newScale);

            Mat4f temp = Mat4f.Product(translateMatrix, scaleMatrix);
            objectMVP = Mat4f.Product(temp, rotationMatrix);
            for(Transform child : children){
                child.scale(newScale);
            }
        }
    }

    public void rotate(float newAngle) {
        if (!isStatic) {
            rotationMatrix = Mat4f.RotateSt(newAngle);

            Mat4f temp = Mat4f.Product(translateMatrix, scaleMatrix);
            objectMVP = Mat4f.Product(temp, rotationMatrix);

            for(Transform child : children){
                child.rotate(newAngle);
            }
        }
    }
    public Mat4f getMVP(){
        return objectMVP;
    }


    public void addChild(Transform transform){
        children.add(transform);
    }

    public Transform getChild(int index){
        try{
            return children.get(index);
        }catch(IndexOutOfBoundsException e){
            return this;
        }
    }

    @Override
    public eComponentType getComponent() {
        return eComponentType.Transform;
    }

}
