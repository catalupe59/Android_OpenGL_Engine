package com.brawlers.cata.brawlers2.obj.obj;

/**
 * Created by Cata on 9/28/2017.
 */

public enum eComponentType {
    Transform(0),
    Mesh(1),
    ParticleSystem(2),
    Collider(3);

    private int ID;
    private eComponentType(int ID){
        this.ID = ID;
    }
    public int getID(){
        return ID;
    }
}
