package com.brawlers.cata.brawlers2.obj.obj;

/**
 * Created by Cata on 9/28/2017.
 */

public interface iComponent {
    eComponentType getComponent();
}
