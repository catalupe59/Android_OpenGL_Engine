package com.brawlers.cata.brawlers2.obj.obj;

import android.opengl.GLES20;
import android.opengl.Matrix;

import com.brawlers.cata.brawlers2.Component.LowLevel.Structs.Mat4f;

/**
 * Created by Cata on 9/8/2017.
 */

public class Camera{
    public static Camera Main;
    public static Camera UICamera;

    private Transform transform;
    private Mat4f projectionMatrix;

    public Camera(int width, int height){
//        setOrthographic(width, height, 10.0f, 0.1f);
        float aspect = (float)width/(float)height;
        setPerspective(60, aspect, 1f, 100.0f);
    }

    public void setTransform(Transform transform){
        this.transform = transform;
    }

    public void setPerspective(float angle, float aspect, float near, float far){
        float f = (float)Math.tan(Math.PI * 0.5 - 0.5 * angle);
        float range = 1.0f/(near-far);

        float[] mat = new float[16];
        Matrix.frustumM(mat,0,-aspect, aspect, -1,1,1,100);
        projectionMatrix = new Mat4f(mat);

//        projectionMatrix = new Mat4f(new float[]{
//                f/aspect, 0 , 0, 0,
//                0, f, 0, 0,
//                0, 0, (near+far)*range,near*far*range*2,
//                0, 0,  -1, 0
//        });

    }

    public void setOrthographic(float width, float height, float far, float near){
        float left = -width/2;
        float right = width/2;
        float top = height/2;
        float bottom = -height/2;

        projectionMatrix = new Mat4f(new float[]{
                2.0f/(right-left), 0, 0, (right+left)/(left-right),
                0, 2.0f/(top-bottom), 0, (top + bottom)/(bottom-top),
                0, 0, 2.0f/(near-far), (far+near)/(far-near),
                0,0,0,1
        });
    }

    public Mat4f getProjectionMatrix(){
        return projectionMatrix;
    }
    public Transform getTransform(){
        return transform;
    }

}
