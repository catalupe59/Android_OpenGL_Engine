package com.brawlers.cata.brawlers2.obj;

/**
 * Created by Cata on 9/28/2017.
 */

public abstract class Obj {
    private static int globalID;
    protected final int ID;
    public Obj(){
        ID = globalID;
        globalID++;
    }

    @Override
    public boolean equals(Object obj) {
        if (((Obj)obj).ID == this.ID){
            return true;
        }
        return false;
    }
}
