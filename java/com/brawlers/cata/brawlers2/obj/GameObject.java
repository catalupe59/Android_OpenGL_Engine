package com.brawlers.cata.brawlers2.obj;

import com.brawlers.cata.brawlers2.obj.obj.Camera;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.Mesh;
import com.brawlers.cata.brawlers2.obj.obj.Mesh.RenderType;
import com.brawlers.cata.brawlers2.obj.obj.Transform;
import com.brawlers.cata.brawlers2.obj.obj.eComponentType;
import com.brawlers.cata.brawlers2.obj.obj.iComponent;

import java.util.HashMap;

/**
 * Created by Cata on 9/28/2017.
 */

public class GameObject extends Obj {
    public HashMap<eComponentType,iComponent> components;

    public GameObject(){
        super();
        components = new HashMap<>();
    }

    public GameObject(iComponent... component){
        super();
        components = new HashMap<>();
        addComponent(component);
    }

    public void addComponent(iComponent... component){
        for (int i = 0; i < component.length; i++){
            this.components.put(component[i].getComponent(), component[i]);
        }
    }
    public void removeComponent(eComponentType type){
        this.components.remove(type);
    }
    public <T> T getComponent(eComponentType type){
        return (T)this.components.get(type);
    }

    public void render(){
        ((Mesh)getComponent(eComponentType.Mesh)).render(RenderType.TRIANGLES, Camera.Main, (Transform)getComponent(eComponentType.Transform));
    }
}